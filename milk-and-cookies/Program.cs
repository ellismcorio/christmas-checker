﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace milk_and_cookies
{
    class Program
    {
        static void Main(string[] args)
        {
            string christmas = "25/12";

            while (true)
            {
                Console.WriteLine("Enter date:");
                string line = Console.ReadLine();
                if (line == "exit") // Check string
                {
                    break;
                }
                if (line.Contains(christmas))
                {
                    Console.WriteLine("Get the cookies and milk out because it is Christmas!");
                }
                else
                {
                    Console.WriteLine("Put the cookies and milk away because it is not christmas!");
                }
            }
        }
    }
}
